import React, {useState,useEffect} from 'react'
import config from 'visual-config-exposer'
import Phaser, { Game } from 'phaser'
import { times } from 'lodash'

export default function Easy1() {
 
    const [time, setTime] = useState(60)
    const [number, setNumber]=useState(0)
    
    let score=0
    let count=1
    let counts=1
    let counting=1

 
        
       
    //Create new scene
    let gameScreen = new Phaser.Scene('Game')
    let cursors
    let start
    let car
    let grass1
    let finish
    let finish2
    let finish3
   // let points
  //  let points1
    let point31
    let road2
    let road3
    let house1
    let roa
    let backmusic
    let direction
    let direction2
    let point3
   let level='Easy'
   let win=1
   let grass2
   let building
   let hello=1
    

     gameScreen.preload = function() {
        this.load.image('background',config.PreGameScreen.BackgroundImage)
        this.load.audio('backmusic',config.Game.Music)
        this.load.image('car',config.Game.ObjectImage)
        this.load.image('road','assets/roadlayout.png')
        this.load.image('finish',config.Game.DestinationImage)
        this.load.image('finish2',config.Game.DestinationImage)
        this.load.image('finish3',config.Game.DestinationImage)
        this.load.image('direction', './assets/direction.png')
        this.load.image('direction2', './assets/direction.png')
      //  this.load.image('points','./assets/star.png')   
      // this.load.image('points1', './assets/star.png')  
        this.load.image('road2','./assets/road.png')
        this.load.image('star','./assets/star.png')
        this.load.image('road3','./assets/road.png')
        this.load.image('roa','./assets/road.png')
      //  this.load.image('point3','./assets/star.png') 
        this.load.image('house1','./assets/house1.png')
        this.load.image('start','./assets/start.png')
        this.load.image('grass1','./assets/grass1.png')
        this.load.image('grass2','./assets/grass2.png')
        this.load.image('building','./assets/builiding2.png')
       
    }
    
    
    
    //called after preload ends
    gameScreen.create = function() {
       
      
       if(hello==1){

       

        
        //create background sprite
       let bg=this.add.sprite(0,0,'background')
        bg.setOrigin(0,0) //creates top left corner as origin    
        bg.setDisplaySize(640,360)
        let road = this.physics.add.sprite(0,10,'road').setOrigin(0,0).setScale(0.6,0.7)
     backmusic=this.sound.add('backmusic')
   
    
   backmusic.loop=true
  
        car = this.physics.add.sprite(0,40,'car').setScale(0.7).setCollideWorldBounds(true);
         car.depth=2;
        car.setDisplaySize(50,25)
        direction = this.physics.add.sprite(70,40,'direction').setDisplaySize(20,20)
        direction2 = this.physics.add.sprite(70,40,'direction2').setDisplaySize(20,20)
        finish = this.physics.add.sprite(510,225,'finish').setOrigin(0,0).setScale(0.2)
        finish.setDisplaySize(150,100)
        house1 = this.physics.add.sprite(265,150,'house1').setScale(0.18).setAngle()
        start= this.physics.add.sprite(50,100,'start').setScale(0.15)
        grass1=this.physics.add.sprite(450,220,'grass1').setScale(0.1,0.07)
     //   points= this.physics.add.sprite(356,40,'points').setScale(0.05)
      //  points.depth=2
      //  points1=this.physics.add.sprite(356,275,'points1').setScale(0.05)
      // points1.depth=2
       
       /* this.physics.add.collider(car, points)
        this.physics.add.collider(car, points1)*/
     //  this.physics.add.overlap(car, points, hidepoint, null)
     // this.physics.add.overlap(car, points1, hide1, null)
       this.physics.add.overlap(car, finish, add,null)
      
         cursors = gameScreen.input.keyboard.createCursorKeys()   

         function hidepoint() {
             
       points.disableBody(true, true)
      
         // score=score +20;
        
            showScore()
          
          
       }
        
        function add(){
            //setNumber(60)
            
            if(win==1){
               // score=score+20
              
               showScore()
                road.destroy()
                finish.destroy()
                
            }    
        }
        function hide1() {
       
         points1.disableBody(true, true)
      
        console.log('hello2')
       
            showScore()
            
        
      //  score=score+20
    hello++
     }
    }
    else{
        backmusic.pause()
    }
    } 

    function level2() {
     
        setTimeout(()=>{
                   
           
             level='Medium'
             direction.visible=true
    direction.setPosition(50,50).setAngle(90)
             direction.setDisplaySize(20,20)
             direction.depth=1
             direction2.depth=1
              road2=gameScreen.physics.add.sprite(50,80,'road2').setScale(0.5, 0.5) 
              building=gameScreen.physics.add.sprite(550,140,'building').setDisplaySize(100,100)
             finish2 = gameScreen.physics.add.sprite(550, 260,'finish2').setScale(0.2) 
        finish2.setDisplaySize(150,100)
             grass2=gameScreen.physics.add.sprite(40,220,'grass2').setScale(0.01)
             grass2.setDisplaySize(70,80)
          
            
             /*road2.setAngle(90)*/
             for(var i=1;i<3;i++){
              road3=gameScreen.physics.add.sprite(280,i*140,'road3').setScale(0.5, 1.2).setAngle(90)  
             
            
             }
             start.setPosition(150,50)
             road3.setAngle(90)
             grass1.setPosition(270,210)
             house1.setPosition(350,50)
            // points.setPosition(50,50)
             
             car.setPosition(50,0)
             car.setAngle(90)
             
            
            roa = gameScreen.add.group()
            roa.enableBody=true;

           
                let roal = roa.create(150, 210, 'roa')
                
                roal.setScale(0.5,0.3)
                let short3 = roa.create(400, 210, 'roa')
                
                short3.setScale(0.5,0.3)
                let short2 = roa.create(250,50, 'roa')
                 short2.setScale(0.5,0.4)
                win=2
               
               
                gameScreen.physics.add.overlap(car, finish2, level3, null)
                
                function level3() {
                    finish2.visible=false
                  backmusic.play()
                    showScore()
                    score=90
                  //  setTimeout(()=>{
                        count=1
                     level='Difficult'
                     short2.destroy()
                     short3.destroy()
                     roal.destroy()
                     car.body.velocity.x=0
                     car.setPosition(50,0)
                     car.setAngle(90)
                     house1.setPosition(550, 150 )
                     start.setPosition(130,50)
                     grass1.setScale(0.05)
                     grass1.setPosition(350,220)
                     grass2.destroy()
                     grass2.setPosition(150,220)
                     grass2.setScale(0.007)
                   
                    /*let road31= roa.create(100, 200, 'roa')
                    road31.setScale(0.3)*/
                    finish3 = gameScreen.physics.add.sprite(550, 260,'finish3')
                 finish3.setScale(0.2)
                  finish3.setDisplaySize(150,100)
                    let road32=roa.create(300,50,'roa')
                    road32.setAngle(90)
                    road32.setScale(0.4,0.5)
                   
            
                   let road33=roa.create(200,50,'roa')
                    road33.setScale(0.4)
            
                    let road34=roa.create(400,50,'roa')
                    road34.setScale(0.4)
            
                    for(var i =1; i<5; i++){ 
                    let road35=roa.create(i*100,210,'roa')
                    road35.setScale(0.4,0.3)
                    gameScreen.physics.add.overlap(car, finish2, end, null)
                   
                    }
                   
                 //   },1000)
                  
               }
               },1000)
           
     }

    
   gameScreen.update = function() {

   
if(level=='Easy'){


    // points.angle+=1
    // points1.angle+=1
    if(cursors.right.isDown && count==1){
        count=10
        direction.setAngle(0)
        direction.visible=false
        direction2.visible=false
        car.setAngle(0)
        car.body.velocity.x=85
        setTimeout(()=> {
            car.body.velocity.x=0
            direction.setPosition(220,40)
            direction2.setPosition(175,70)
            direction.visible=true
            direction2.setAngle(90)
            direction2.visible=true
            
            count=2
        },1800)
        
    }

        if(cursors.left.isDown && count==2){
           count=10
       hideDirection()
         car.setAngle(180) 
         car.body.velocity.x=-75
         setTimeout(()=> {
            car.body.velocity.x=0
            if(car.body.velocity.x==0){
                count=1
            }
           
        },2000)
        
        }

       
    if(cursors.down.isDown && count==2){
        count=10
        hideDirection()
        car.setAngle(90)
        car.body.velocity.y=75
        setTimeout(()=> {
             car.body.velocity.y=0
             direction.setPosition(210,280).setAngle(0)
             direction.visible=true
             count=3
          },3200)
        
         }

         if(cursors.up.isDown && count==3){
            count=10
             hideDirection()
            car.setAngle(-90)
            car.body.velocity.y=-75
            setTimeout(()=> {
                car.body.velocity.y=0
                direction.setPosition(210,40)
            direction2.setPosition(150,40)
            direction.visible=true
            direction2.setAngle(1800)
            direction2.visible=true
                count=2
            },3200)
           
            }


        if(cursors.right.isDown && count==2){
            count=10
            hideDirection()
            car.setAngle(0)
            car.body.velocity.x=75
            setTimeout(()=> {
                car.body.velocity.x=0
               direction.visible=true
               direction.setPosition(355,70).setAngle(90)
                count=4
            },2400)
        
        }
        if(cursors.left.isDown && count==4){
            count=10
       hideDirection()

            car.setAngle(180) 
            car.body.velocity.x=-85
            setTimeout(()=> {
               car.body.velocity.x=0
               showDirection()
               direction.setPosition(125,40).setAngle(180)
               direction2.setPosition(175,70)
              
               direction2.setAngle(90)
              
               
                count=2
           },2100)
          
           }


           if(cursors.down.isDown && count==4){
            count=10
               hideDirection()
               
            car.setAngle(90)
            car.body.velocity.y=75
            setTimeout(()=> {
                car.body.velocity.y=0
                direction.visible=true
                direction2.visible=true
                direction.setPosition(385,280).setAngle(0)
               direction2.setPosition(330,280).setAngle(180)
                count=5
            },3200)
        
        }

        if(cursors.up.isDown && count==5){
            count=10
            hideDirection()
            car.setAngle(270)
            car.body.velocity.y=-75
            setTimeout(()=> {
                car.body.velocity.y=0
                count=4
            },3200)
        
        }
       

         if(cursors.right.isDown && count==3){
            count=10
            direction.visible=false
          car.setAngle(0)
             car.body.velocity.x=75
             setTimeout(()=> {
             car.body.velocity.x=0
             direction.visible=true
             direction.setPosition(395,280).setAngle(0)
     direction2.setPosition(355,250).setAngle(270)
     direction2.visible=true
             count=5
         },2350)
  }

                   if(cursors.left.isDown && count==5){
                    count=10
                    car.setAngle(180)
                    hideDirection()
                       car.body.velocity.x=-75
                       setTimeout(()=> {
                       car.body.velocity.x=0
                       count=3
                   },2400)
                        
                             }
                             if(cursors.right.isDown && count==5){
                                count=10
                                 hideDirection()
                                car.setAngle(0)
                                   car.body.velocity.x=75
                                   setTimeout(()=> {
                                   car.body.velocity.x=0
                                   count=6
                               },2500)
                        }
                            

                }   

if(level=='Medium'){
    
    
   
       if(cursors.down.isDown && counts==1){
           counts=10
         hideDirection()
           car.setAngle(90)
           car.body.velocity.y=75
           setTimeout(()=> {
               car.body.velocity.y=0
               car.setAngle(0)
           car.body.velocity.x=65
           setTimeout(() => {
            car.body.velocity.x=0
            direction.visible=true
            direction2.visible=true
            direction.setPosition(190,140).setAngle(0)
            direction2.setPosition(150,170).setAngle(90)
            counts=2
        }, 1500)
        
           },1700)
        
           }

           if(cursors.left.isDown && counts==2){
               hideDirection()
            counts=10
            car.setAngle(180)
            car.body.velocity.x=-60
            setTimeout(() => {
                car.body.velocity.x=0
                car.setAngle(270)
                car.body.velocity.y=-75
                setTimeout(() => {
                    car.body.velocity.y=0
                    counts=1
                }, 1800);
                
            }, 1600);   
    
           
           }
        if(cursors.down.isDown && counts==2) {
            counts=10
            hideDirection()
            car.setAngle(90)
            car.body.velocity.y=70
            setTimeout(() => {
                car.body.velocity.y=0
                car.setAngle(0)
                car.body.velocity.x=100
               setTimeout(() => {
                   car.body.velocity.x=0
                   direction.visible=true
                   direction2.visible=true
                   direction.setPosition(440,280).setAngle(0)
                   direction2.setPosition(400,245).setAngle(270)
                   counts=3
               }, 2500)
              
            }, 2000)
           
        }

         if(cursors.left.isDown && counts==3) {
            counts=10
             hideDirection()
            car.setAngle(180)
            car.body.velocity.x=-100
            setTimeout(() => {
                car.body.velocity.x=0
                car.setAngle(270)
                car.body.velocity.y=-75
               setTimeout(() => {
                   car.body.velocity.y=0
                   showDirection()
                   direction.setPosition(150,140).setAngle(0)
                   direction2.setPosition(120,140).setAngle(180)
                   counts=2
               }, 2000)
             
            }, 2600)
           
        }

        if(cursors.up.isDown && counts==3){
            counts=10
           hideDirection()
           car.setAngle(270)
            car.body.velocity.y=-85
            setTimeout(() => {
                car.body.velocity.y=0
              
               direction.setPosition(430,140).setAngle(0)
               direction2.setPosition(350,140).setAngle(180)
                showDirection()
                counts=5
            }, 1700)
        }
        if(cursors.right.isDown && counts==2){
            counts=10
           hideDirection()
           car.setAngle(0)
            car.body.velocity.x=85
            setTimeout(() => {
                car.body.velocity.x=0
              
               direction.setPosition(300,140).setAngle(0)
               direction2.setPosition(250,100).setAngle(270)
                showDirection()
                counts=4
            }, 1250)
        }

        if(cursors.up.isDown && counts==4){
            counts=10
           hideDirection()
           car.setAngle(270)
            car.body.velocity.y=-85
            setTimeout(() => {
                car.body.velocity.y=0
              
              
                counts=31
            }, 1550)
        }

        if(cursors.down.isDown && counts==31){
            counts=10
           hideDirection()
           car.setAngle(90)
            car.body.velocity.y=85
            setTimeout(() => {
                car.body.velocity.y=0
              direction2.setPosition(280,140).setAngle(0)
              direction.setPosition(230,140).setAngle(180)
              showDirection()
                counts=4
            }, 1550)
        }

        if(cursors.left.isDown && counts==4){
            counts=10
            hideDirection()
            
            car.setAngle(180)
             car.body.velocity.x=-85
             setTimeout(() => {
                 car.body.velocity.x=0
                 showDirection()
                 direction.setPosition(100,140).setAngle(180)
            direction2.setPosition(150,180).setAngle(90)
                 counts=2
             }, 1250)
            
         }

        if(cursors.right.isDown && counts==3){
            counts=10
            hideDirection()
            car.body.velocity.x=75
            setTimeout(() => {
               car.body.velocity.x=0 
            },2000);
         
        }
       
        if(cursors.right.isDown && counts==4) {
            counts=10
            hideDirection()
            car.setAngle(0)
            car.body.velocity.x=75
            setTimeout(() => {
                car.body.velocity.x=0
                direction.setPosition(450,140).setAngle(0)
                direction2.setPosition(400,170).setAngle(90)
                showDirection()
               
                counts=5
            }, 2000)
           
        }
        if(cursors.left.isDown && counts==5) {
            counts=10
            hideDirection()
            car.setAngle(180)
            car.body.velocity.x=-75
            setTimeout(() => {
                car.body.velocity.x=0
                showDirection()
                direction.setPosition(210,140).setAngle(180)
                direction2.setPosition(250,100).setAngle(270)
                counts=4
            }, 2000)
           
        }
       
        if(cursors.down.isDown && counts==5) {
            counts=10
            hideDirection()
            car.setAngle(90)
            car.body.velocity.y=75
            setTimeout(() => {
                car.body.velocity.y=0
                showDirection()
                direction.setPosition(430,280).setAngle(0)
               direction2.setPosition(380,280).setAngle(180)
               counts=6
            }, 1800)
           
        }
        if(cursors.up.isDown && counts==6) {
            counts=10
            hideDirection()
            car.setAngle(270)
            car.body.velocity.y=-75
            setTimeout(() => {
                car.body.velocity.y=0
                counts=5
            }, 1800)
           
        }

        if(cursors.right.isDown && counts==6) {
            counts=10
            hideDirection()
            car.setAngle(0)

            car.body.velocity.x=85
            setTimeout(() => {
                car.body.velocity.x=0
               counts=7
            }, 2500)

        }
    
}

            if(score==60 && win==1) {
              
            level2()
            
            }
            if( score==90 && win==2){
          
                if(cursors.down.isDown && counting==1){
                    counting=10
                    car.setAngle(90)
                    car.body.velocity.y=65
                                    setTimeout(() => {
                                        car.body.velocity.y=0
                                        car.setAngle(0)
                                        car.body.velocity.x=75
                                        setTimeout(() => {
                                            car.body.velocity.x=0
                                            counting=2
                                            showDirection()
                                            direction.setPosition(150,140)
                                            direction2.setPosition(100,180).setAngle(90)
                            
                                        }, 700);
                                       
                                       
                                    }, 1900);                                        
                }


                if(cursors.left.isDown && counting==2){
                    counting=10
                    hideDirection()
                    car.setAngle(180)
                    car.body.velocity.x=-75
                                    setTimeout(() => {
                                        car.body.velocity.x=0
                                        car.setAngle(270)
                                        car.body.velocity.y=-65
                                        setTimeout(() => {
                                            car.body.velocity.y=0
                                            counting=1
                                        }, 900);
                                        
                                       
                                    }, 1900); 
                }
            
                //count 2 second turn
                if(cursors.down.isDown && counting==2){
                    counting=10
                    hideDirection()
                    car.setAngle(90)
                    car.body.velocity.y=75
                    setTimeout(()=>{
                        car.body.velocity.y=0
                       
                        counting=3
                        direction.visible=true
                        direction.setPosition(140,280).setAngle(0)
                    },1700)

                }

                
                if(cursors.up.isDown && counting==3){
                    hideDirection()
                    counting=10
                    car.setAngle(270)
                    car.body.velocity.y=-75
                    setTimeout(()=>{
                        car.body.velocity.y=0
                        counting=2
                       showDirection()
                       direction.setPosition(150,140).setAngle(0)
                       direction2.setPosition(50,140).setAngle(180)
            
                    },1800)
                    
            
                }
                if(cursors.right.isDown && counting==3){
                    counting=10
                    hideDirection()
                    car.setAngle(0)
                    car.body.velocity.x=75
                    setTimeout(()=>{
                        car.body.velocity.x=0
                       
                        counting=21
                        direction2.setPosition(250,280).setAngle(0)
                        direction.setPosition(200,240).setAngle(270)
                       // direction2.setPosition(200,180).setAngle(90)
                        showDirection()
                       
                    },1300)

                }

                if(cursors.left.isDown && counting==21){
                    counting=10
                    hideDirection()
                    car.setAngle(180)
                    car.body.velocity.x=-75
                    setTimeout(()=>{
                        car.body.velocity.x=0
                       car.setAngle(270)
                       car.body.velocity.y=-75
                       setTimeout(() => {
                           car.body.velocity.y=0
                           counting=2
                          
                       direction.setPosition(150,140).setAngle(0)
                       direction2.setPosition(50,140).setAngle(180)
                       showDirection()
                       }, 1300);
                        
                        
                       // direction2.setPosition(200,180).setAngle(90)
                       
                       
                    },1800)

                }

                if(cursors.up.isDown && counting==21){
                    
                    counting=10
                    hideDirection()
                    car.setAngle(270)
                    car.body.velocity.y=-75
                    setTimeout(()=>{
                        car.body.velocity.y=0
                       
                        counting=5
                        direction.setPosition(150,140).setAngle(180)
                        direction2.setPosition(250,140).setAngle(0)
                        showDirection()
                       
                    },1800)

                }

               

             
                if(cursors.right.isDown && counting==21){
                    counting=10
                    hideDirection()
                    car.setAngle(0)
                    car.body.velocity.x=75
                    setTimeout(()=>{
                        car.body.velocity.x=0
                       
                        counting=22
                        direction2.setPosition(350,280).setAngle(0)
                        direction.setPosition(300,240).setAngle(270)
                       // direction2.setPosition(200,180).setAngle(90)
                        showDirection()
                       
                    },1300)

                }

                if(cursors.left.isDown && counting==22){
                    counting=10
                    hideDirection()
                    car.setAngle(180)
                    car.body.velocity.x=-75
                    setTimeout(()=>{
                        car.body.velocity.x=0
                       
                        counting=21
                        direction2.setPosition(150,280).setAngle(180)
                        direction.setPosition(200,240).setAngle(270)
                       // direction2.setPosition(200,180).setAngle(90)
                        showDirection()
                       
                    },1300)

                }

                if(cursors.up.isDown && counting==22){
                    
                    counting=10
                    hideDirection()
                    car.setAngle(270)
                    car.body.velocity.y=-75
                    setTimeout(()=>{
                        car.body.velocity.y=0
                       
                        counting=6
                        direction.setPosition(250,140).setAngle(180)
                        direction2.setPosition(350,140).setAngle(0)
                        showDirection()
                       
                    },1800)

                }

                
                if(cursors.right.isDown && counting==22){
                    counting=10
                    hideDirection()
                    car.setAngle(0)
                    car.body.velocity.x=75
                    setTimeout(()=>{
                        car.body.velocity.x=0
                       
                        counting=23
                        direction2.setPosition(450,280).setAngle(0)
                        direction.setPosition(400,240).setAngle(270)
                       // direction2.setPosition(200,180).setAngle(90)
                        showDirection()
                       
                    },1300)

                }

                if(cursors.up.isDown && counting==23){
                    
                    counting=10
                    hideDirection()
                    car.setAngle(270)
                    car.body.velocity.y=-75
                    setTimeout(()=>{
                        car.body.velocity.y=0
                       
                        counting=7
                        direction.setPosition(350,140).setAngle(180)
                        direction2.setPosition(450,140).setAngle(0)
                        showDirection()
                       
                    },1800)

                }


                if(cursors.left.isDown && counting==23){
                    counting=10
                    hideDirection()
                    car.setAngle(180)
                    car.body.velocity.x=-75
                    setTimeout(()=>{
                        car.body.velocity.x=0
                       
                        counting=22
                        direction2.setPosition(250,280).setAngle(180)
                        direction.setPosition(300,240).setAngle(270)
                       // direction2.setPosition(200,180).setAngle(90)
                        showDirection()
                       
                    },1300)

                }

                if(cursors.right.isDown && counting==23){
                    counting=10
                    hideDirection()
                    car.setAngle(0)
                    car.body.velocity.x=75
                    setTimeout(()=>{
                        car.body.velocity.x=0
                  
                    },1300)

                }

              /*  if(cursors.left.isDown && counting==3){
                    car.setAngle(90)
                    car.body.velocity.x=-75
                    setTimeout(()=>{
                        car.body.velocity.x=0
                        setAngle(270)
                        car.body.velocity.y=-75
                        setTimeout(() => {
                            car.body.velocity.y=0 
                            counting=2  
                        }, 2000);
                    
                    },1000)
            
                }*/
            
                //for right
                if(cursors.right.isDown && counting==2){
                    hideDirection()
                    counting=10
                    car.body.velocity.x=75
                    car.setAngle(0)
                    setTimeout(() => {
                        car.body.velocity.x=0
                        counting=5
                        showDirection()
                        direction.setPosition(200,90).setAngle(270)
                        direction2.setPosition(200,180).setAngle(90)
                    }, 1300);
                }

                if(cursors.down.isDown && counting==5){
                    hideDirection()
                    counting=10
                    car.body.velocity.y=75
                    car.setAngle(90)
                    setTimeout(() => {
                        car.body.velocity.y=0
                        counting=21
                        showDirection()
                        direction.setPosition(170,280).setAngle(180)
                        direction2.setPosition(230,280).setAngle(0)
                    }, 1800);
                }
            
                if(cursors.left.isDown && counting==5){
                    hideDirection()
                    counting=10
                    car.body.velocity.x=-75
                    car.setAngle(180)
                    setTimeout(() => {
                        car.body.velocity.x=0
                        counting=2
                       
                        direction.setPosition(50,140).setAngle(180)
                        direction2.setPosition(100,180).setAngle(90)
                        showDirection()
                    }, 1500);
                }

                if(cursors.right.isDown && counting==5){
                    hideDirection()
                    counting=10
                    car.body.velocity.x=75
                    car.setAngle(0)
                    setTimeout(() => {
                        car.body.velocity.x=0
                        counting=6
                        direction.setPosition(350, 140).setAngle(0)
                        direction2.setPosition(300,180).setAngle(90)
                        showDirection()
                    }, 1500);
                }


                if(cursors.down.isDown && counting==6){
                    hideDirection()
                    counting=10
                    car.body.velocity.y=75
                    car.setAngle(90)
                    setTimeout(() => {
                        car.body.velocity.y=0
                        counting=22
                        showDirection()
                        direction.setPosition(270,280).setAngle(180)
                        direction2.setPosition(330,280).setAngle(0)
                    }, 1800);
                }
            

                if(cursors.left.isDown && counting==6){
                    hideDirection()
                    counting=10

                    car.body.velocity.x=-75
                    car.setAngle(180)
                    setTimeout(() => {
                        car.body.velocity.x=0
                        counting=5
                        direction.setPosition(160,140).setAngle(180)
                        direction2.setPosition(200,180).setAngle(90)
                        showDirection()
                    }, 1500);
                }

               if(cursors.right.isDown && counting==6){
                   hideDirection()
                   counting=10
                    car.body.velocity.x=75
                    car.setAngle(0)
                    setTimeout(() => {
                        car.body.velocity.x=0
                        counting=7
                        direction.setPosition(450, 140).setAngle(0)
                        direction2.setPosition(400,180).setAngle(90)
                        showDirection()
                    }, 1500);
                }
                if(cursors.down.isDown && counting==7){
                    hideDirection()
                    counting=10
                    car.body.velocity.y=75
                    car.setAngle(90)
                    setTimeout(() => {
                        car.body.velocity.y=0
                        counting=23
                        showDirection()
                        direction.setPosition(370,280).setAngle(180)
                        direction2.setPosition(430,280).setAngle(0)
                    }, 1800);
                }
                if(cursors.left.isDown && counting==7){
                    hideDirection()
                    counting=10
                     car.body.velocity.x=-75
                     car.setAngle(180)
                     setTimeout(() => {
                         car.body.velocity.x=0
                         counting=6
                         direction.setPosition(270, 140).setAngle(180)
                         direction2.setPosition(300,180).setAngle(90)
                         showDirection()
                     }, 1500);
                 }
 

         if(cursors.right.isDown && counting==7){
             hideDirection()
                    car.body.velocity.x=75
                    car.setAngle(0)
                    setTimeout(() => {
                        car.body.velocity.x=0
                        counting=8
                    }, 1500);
                }

                if(cursors.left.isDown && counting==8){
                    
                           car.body.velocity.x=-75
                           car.setAngle(0)
                           setTimeout(() => {
                               car.body.velocity.x=0
                               counting=7
                               direction.setPosition(300, 140).setAngle(180)
                               showDirection
                           }, 1500);
                       }
            
                //count=3
            //for right
  /*          if(cursors.right.isDown && counting==3){
                    car.body.velocity.x=75
                    car.setAngle(0)
                    setTimeout(() => {
                        car.body.velocity.x=0
                        counting=4
                    }, 1000);
                }
                if(cursors.left.isDown && counting==4){
                    car.setAngle(180)
                    car.body.velocity.x=-75
                    
                    setTimeout(() => {
                        car.body.velocity.x=0
                        counting=3
                    }, 1000);
                }
                //for up
                if(cursors.up.isDown && counting==3){
                    car.body.velocity.y=-75
                    car.setAngle(270)
                    setTimeout(() => {
                        car.body.velocity.y=0
                        counting=5
                    }, 2000);
                }
                if(cursors.down.isDown && counting==5){
                    car.body.velocity.y=+75
                    car.setAngle(90)
                    setTimeout(() => {
                        car.body.velocity.y=0
                        counting=3
                    }, 2000);
                }*/
            
                if(car.x>=finish.x){
                    showScore()
                    end()
                  
                }
                //count 4
            }           
        
}

    //set the configuration of the game
    let configuration = {
        type: Phaser.AUTO,
        width: 640, //640,
        height: 360,//360,
        scene: gameScreen,
        physics: {
            default: 'arcade'
        }
       
        
    };

    // create a game, passing configuration
    let game = new Phaser.Game(configuration);
/*document.getElementById('phaser').style.visibility='hidden'*/

  //  });
    
    
function end() {
    gameScreen.sound.stopAll()
    setNumber(120)
    document.getElementsByClassName('postGame')[0].style.visibility='visible'
    document.getElementById('background').style.visibility='visible' 
}
    function hide() {
          document.getElementsByClassName('pregame')[0].style.visibility='hidden'
          document.getElementsByClassName('Instructions')[0].style.visibility='visible'
          backmusic.play()
    }
    function playagain(){
window.location.reload(true)
    }
  function show() {
    document.getElementsByClassName('Instructions')[0].style.visibility='hidden'

    document.getElementById('background').style.visibility='hidden'
  }
 function checkTiles() {
     setTime(time-1)
 }
 function showScore() {
     score=score+60
     setNumber(score)
 }
 function hideDirection(){
     direction.visible=false
     direction2.visible=false
 }

 function showDirection(){
     direction.visible=true
     direction2.visible=true
 }

    return(
        <>
       <h1 id='phaser'>Score:{number}</h1>
        <div id='background'>
        <div className='prescreen'>
            <div className='pregame'>
                <img src={config.PreGameScreen.GameLogo}></img>
    <h2>{config.PreGameScreen.GameName}</h2>
               <button onClick={hide}>{config.PreGameScreen.PlayButtonText}</button>
    <button>{config.PreGameScreen.LeaderBoardText}</button>
    </div>
    </div>
        </div>

        <div className='Instructions'>
            <h2>Instructions</h2>
            <h5>1. Press arrow keys to move the object.</h5>
            <h5>2. Collect all the points.</h5>
            <button onClick={show}>OK</button>
        </div>

        
        <div className='postGame'>
    <h2>Your score is:{number}</h2>
    <button onClick={playagain}>{config.PostScreen.againButtonText}</button>
            <div>
    <h2>{config.PostScreen.ctaText}</h2>
                <button><a href={config.PostScreen.ctaUrl}>{config.PostScreen.ctaBtnText}</a></button>
            </div>

        </div>
        </>
    )
    }
  
